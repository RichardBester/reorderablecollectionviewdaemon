//
//  ReorderableCollectionView.m
//  ReorderableCollectionViewDeamon
//
//  Created by Sauron Black on 8/20/14.
//  Copyright (c) 2014 ThinkMobiles. All rights reserved.
//

#import "ReorderableCollectionView.h"
#import "ReorderableCollectionViewFakeCell.h"
#import "ReorderableCollectionViewLayoutProtocol.h"

#define ReorderableCollectionViewDefaultScrollingEdgeInsets UIEdgeInsetsMake(50.f, 50.f, 50.f, 50.f)

static CGFloat const ReorderableCollectionViewDefaultVelocity = 300.f;

typedef NS_ENUM(NSInteger, ReorderableCollectionViewScrollDirection) {
    ReorderableCollectionViewScrollDirectionUnknown,
    ReorderableCollectionViewScrollDirectionUp,
    ReorderableCollectionViewScrollDirectionDown
};

@interface ReorderableCollectionView ()

@property (strong, nonatomic) ReorderableCollectionViewFakeCell *fakeCell;
@property (strong, nonatomic) UILongPressGestureRecognizer      *longPressGesture;
@property (strong, nonatomic) NSIndexPath                       *fromIndexPath;
@property (strong, nonatomic) NSIndexPath                       *toIndexPath;
@property (strong, nonatomic) CADisplayLink                     *scrollTimer;

@property (assign, nonatomic) CGVector                                  fakeCellCenterOffset;
@property (assign, nonatomic) ReorderableCollectionViewScrollDirection  reorderableScrollDirection;

@property (weak, nonatomic) UICollectionViewCell *engagedCell;

@end

@implementation ReorderableCollectionView

#pragma mark - Lifecycle

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

#pragma mark - Custom Accessors

- (void)setReorderingEnabled:(BOOL)reorderingEnabled {
    _reorderingEnabled = reorderingEnabled;
    
    self.longPressGesture.enabled = _reorderingEnabled;
}

#pragma mark - Private

- (void)setup {
    self.longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressGestureAction:)];
    self.longPressGesture.minimumPressDuration = 1.f;
    [self addGestureRecognizer:self.longPressGesture];
    
    self.reorderingEnabled = YES;
    self.scrollingInsets = ReorderableCollectionViewDefaultScrollingEdgeInsets;
    self.velocity = ReorderableCollectionViewDefaultVelocity;
}

- (void)instantiateScrollTimerWithScrollDirection:(ReorderableCollectionViewScrollDirection)scrollDirection {
    self.reorderableScrollDirection = scrollDirection;
    
    if (!self.scrollTimer) {
        self.scrollTimer = [CADisplayLink displayLinkWithTarget:self selector:@selector(scrollTimerFireMethod:)];
        [self.scrollTimer addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
    }
}

- (void)invalidateScrollTimer {
    self.reorderableScrollDirection = ReorderableCollectionViewScrollDirectionUnknown;
    
    if (self.scrollTimer) {
        [self.scrollTimer invalidate];
        self.scrollTimer = nil;
    }
}

- (void)scrollTimerFireMethod:(CADisplayLink *)timer {
    CGFloat distance = self.velocity / 60.f;
    CGPoint location = CGPointMake(self.fakeCell.center.x - self.fakeCellCenterOffset.dx, self.fakeCell.center.y - self.fakeCellCenterOffset.dy);
    
    CGPoint contentOffset;
    CGSize contentSize;
    CGRect frame;
    
    if (self.scrolableContainer) {
        contentOffset = self.scrolableContainer.contentOffset;
        contentOffset.y -= self.scrolableContainerScrollInsets.top;
        contentSize = self.scrolableContainer.contentSize;
        contentSize.height -= (self.scrolableContainerScrollInsets.top - self.scrolableContainerScrollInsets.bottom);
        frame = self.scrolableContainer.frame;
    } else {
        contentOffset = self.contentOffset;
        contentSize = self.contentSize;
        frame = self.frame;
    }
    
    switch (self.reorderableScrollDirection) {
        case ReorderableCollectionViewScrollDirectionUp:
            contentOffset = CGPointMake(contentOffset.x, MAX(0.f, contentOffset.y - distance));
            location.y = contentOffset.y < distance ? location.y: location.y - distance;
            break;
            
        case ReorderableCollectionViewScrollDirectionDown:
            contentOffset = CGPointMake(contentOffset.x, MIN(contentSize.height - CGRectGetHeight(frame), contentOffset.y + distance));
            location.y = (contentSize.height - CGRectGetHeight(frame)) < (contentOffset.y + distance) ? location.y :location.y + distance;
            break;
            
        case ReorderableCollectionViewScrollDirectionUnknown:
            return;
    }
    
    if (self.scrolableContainer) {
        contentOffset.y += self.scrolableContainerScrollInsets.top;
        self.scrolableContainer.contentOffset = contentOffset;
    } else {
        self.contentOffset = contentOffset;
    }
    
    [self changeFakeCellCenterToLocation:location];
    [self handleChangedLocation:location];
}

- (void)handleChangedLocation:(CGPoint)location {
    NSIndexPath *newDestinationIndexPath = [self indexPathForItemClosestToPoint:location];
    
    if (newDestinationIndexPath && ![newDestinationIndexPath isEqual:self.toIndexPath] &&
        [((id<ReorderableCollectionViewDataSource>)self.dataSource) collectionView:self canMoveCellFromIndexPath:self.fromIndexPath toIndexPath:self.toIndexPath]) {
        
        self.toIndexPath = newDestinationIndexPath;
        
        [((id<ReorderableCollectionViewDataSource>)self.dataSource) collectionView:self moveCellFromIndexPath:self.fromIndexPath toIndexPath:self.toIndexPath];
        
        [self moveItemAtIndexPath:self.fromIndexPath toIndexPath:self.toIndexPath];
        self.fromIndexPath = self.toIndexPath;
    }
}

- (NSIndexPath *)indexPathForItemClosestToPoint:(CGPoint)point {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"representedElementCategory == %@", @(UICollectionElementCategoryCell)];
    NSArray *attributesArray = [[self.collectionViewLayout layoutAttributesForElementsInRect:self.bounds] filteredArrayUsingPredicate:predicate];
    NSInteger closestDistance = NSIntegerMax;
    NSIndexPath *indexPath;
    CGRect closestCellFrame;
    
    for (UICollectionViewLayoutAttributes *attributes in attributesArray) {
        CGFloat xd = attributes.center.x - point.x;
        CGFloat yd = attributes.center.y - point.y;
        NSInteger distance = sqrtf(xd * xd + yd * yd);
        if (distance < closestDistance) {
            closestDistance = distance;
            indexPath = attributes.indexPath;
            closestCellFrame = attributes.frame;
        }
    }
    
    if ([self.collectionViewLayout isKindOfClass:[UICollectionViewFlowLayout class]]) {
        UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)self.collectionViewLayout;
        UIEdgeInsets edgeInsts = UIEdgeInsetsMake(-layout.minimumLineSpacing, -layout.minimumInteritemSpacing, -layout.minimumLineSpacing, -layout.minimumInteritemSpacing);
        closestCellFrame = UIEdgeInsetsInsetRect(closestCellFrame, edgeInsts);
        if (!CGRectContainsPoint(closestCellFrame, point)) {
            indexPath = nil;
        }
    }
    
    return indexPath;
}

- (void)changeFakeCellCenterToLocation:(CGPoint)location {
    self.fakeCell.center =  CGPointMake(self.fakeCellCenterOffset.dx + location.x, self.fakeCellCenterOffset.dy + location.y);
}

#pragma mark - Gesture Recognizer Actions

- (void)longPressGestureAction:(UILongPressGestureRecognizer *)sender {
    CGPoint location = [sender locationInView:self];
   
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
            [self longPressGestureBegan:sender loacation:location];
            break;
            
        case UIGestureRecognizerStateChanged:
            [self longPressGestureChanged:sender loacation:location];
            break;
            
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
            [self longPressGestureEnded:sender location:location];
            break;
            
        default:
            NSLog(@"longPressAction.state: %i", sender.state);
            break;
    }
}

- (void)longPressGestureBegan:(UILongPressGestureRecognizer *)sender loacation:(CGPoint)location {
    self.fromIndexPath = [self indexPathForItemAtPoint:location];
    
    if (self.fromIndexPath) {
        self.engagedCell = [self cellForItemAtIndexPath:self.fromIndexPath];
        self.fakeCell = [[ReorderableCollectionViewFakeCell alloc] initWithCell:self.engagedCell collectionView:self];
        self.engagedCell.hidden = YES;
        self.fakeCellCenterOffset = CGVectorMake(self.fakeCell.center.x - location.x, self.fakeCell.center.y - location.y);
    }
}

- (void)longPressGestureChanged:(UILongPressGestureRecognizer *)sender loacation:(CGPoint)location {
    [self changeFakeCellCenterToLocation:location];
    
    CGRect bounds;

    if (self.scrolableContainer) {
        bounds = self.scrolableContainer.bounds;
        bounds.origin.y -= self.scrolableContainerScrollInsets.top;
    } else {
        bounds = self.bounds;
    }
    
    if (location.y <= (CGRectGetMinY(bounds) + self.scrollingInsets.top)) {
        [self instantiateScrollTimerWithScrollDirection:ReorderableCollectionViewScrollDirectionUp];
    } else if (location.y >= (CGRectGetMaxY(bounds) - self.scrollingInsets.bottom)) {
        [self instantiateScrollTimerWithScrollDirection:ReorderableCollectionViewScrollDirectionDown];
    } else {
        [self invalidateScrollTimer];
        
        [self handleChangedLocation:location];
    }
}

- (void)longPressGestureEnded:(UILongPressGestureRecognizer *)sender location:(CGPoint)location {
    if (self.engagedCell) {
        CGPoint newCenter = self.engagedCell.center;
        NSInteger section = [((id<ReorderableCollectionViewLayoutProtocol>)self.collectionViewLayout) sectionAtPoint:location];
        
        if (section >= 0) {
            NSInteger numberOfItems = [self.dataSource collectionView:self numberOfItemsInSection:section];
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:numberOfItems inSection:section];
            
            if ((self.fromIndexPath.section != section) && (self.toIndexPath.section != section) &&
                [((id<ReorderableCollectionViewDataSource>)self.dataSource) collectionView:self canMoveCellFromIndexPath:self.fromIndexPath toIndexPath:indexPath]) {
                
                [((id<ReorderableCollectionViewDataSource>)self.dataSource) collectionView:self
                                                                     moveCellFromIndexPath:self.fromIndexPath
                                                                               toIndexPath:indexPath];
                
                [self moveItemAtIndexPath:self.fromIndexPath toIndexPath:indexPath];
                
                self.fromIndexPath = self.toIndexPath;
                newCenter = [self cellForItemAtIndexPath:indexPath].center;
            }
        }
        
        [self.fakeCell removeWithNewCenter:newCenter completion:^{
            self.engagedCell.hidden = NO;
            self.fakeCell = nil;
            self.fromIndexPath = nil;
        }];
    }
    
    [self invalidateScrollTimer];
}

@end
