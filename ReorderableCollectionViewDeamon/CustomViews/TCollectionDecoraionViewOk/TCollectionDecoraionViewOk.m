//
//  TCollectionDecoraionViewOk.m
//  Fuck
//
//  Created by Sauron Black on 8/18/14.
//  Copyright (c) 2014 ThinkMobile. All rights reserved.
//

#import "TCollectionDecoraionViewOk.h"

@implementation TCollectionDecoraionViewOk

+ (NSString *)kindOfView {
    static NSString *TCollectionDecoraionViewKindOk = @"TCollectionDecoraionViewKindOk";
    return TCollectionDecoraionViewKindOk;
}

#pragma mark - Lifecycle

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.textLabel.text = @"I'm Ok! Das Ist Gut!";
        self.textLabel.textColor = [UIColor colorWithRed:76.f/255.f green:82.f/255.f blue:101.f/255.f alpha:1.f];
        self.backgroundColor = [UIColor colorWithRed:40.f/255.f green:56.f/255.f blue:66.f/255.f alpha:1.f];
    }
    return self;
}

@end
