//
//  ReorderableCollectionViewFakeCell.m
//  ReorderableCollectionViewDeamon
//
//  Created by Sauron Black on 8/20/14.
//  Copyright (c) 2014 ThinkMobiles. All rights reserved.
//

#import "ReorderableCollectionViewFakeCell.h"

@implementation ReorderableCollectionViewFakeCell

#pragma mark - Lifecycle

- (instancetype)initWithCell:(UICollectionViewCell *)cell collectionView:(UICollectionView *)collectionView {
    self = [super initWithFrame:cell.frame];
    if (self) {
        self.image = [self imageFromCell:cell];
        [self showInCollectionView:collectionView center:cell.center];
    }
    return self;
}

#pragma mark - Public

- (void)showInCollectionView:(UICollectionView *)collectionView center:(CGPoint)center {
    [collectionView addSubview:self];
    self.center = center;
    [UIView animateWithDuration:0.3 animations:^{
        self.transform = CGAffineTransformMakeScale(1.1f, 1.1f);
    }];
}

- (void)removeWithNewCenter:(CGPoint)newCenter completion:(void (^)(void))completion {
    [UIView animateWithDuration:0.3f animations:^{
        self.center = newCenter;
        self.transform = CGAffineTransformMakeScale(1.f, 1.f);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        if (completion) {
            completion();
        }
    }];
}

#pragma mark - Private

- (UIImage *)imageFromCell:(UICollectionViewCell *)cell {
    UIGraphicsBeginImageContextWithOptions(cell.bounds.size, cell.isOpaque, 0.0f);
    [cell.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end
