//
//  TribestCollectionViewCell.h
//  Fuck
//
//  Created by Sauron Black on 8/18/14.
//  Copyright (c) 2014 ThinkMobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTribeCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

+ (NSString *)cellReusableIdentifier;

@end
