//
//  ReorderableCollectionView.h
//  ReorderableCollectionViewDeamon
//
//  Created by Sauron Black on 8/20/14.
//  Copyright (c) 2014 ThinkMobiles. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ReorderableCollectionViewDataSource <UICollectionViewDataSource>

@required
- (BOOL)collectionView:(UICollectionView *)collectionView canMoveCellFromIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath;
- (void)collectionView:(UICollectionView *)collectionView moveCellFromIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath;

@end

@interface ReorderableCollectionView : UICollectionView

@property (assign, nonatomic) BOOL reorderingEnabled;
/**
 Edge insets in which scrolling is started
*/
@property (assign, nonatomic) UIEdgeInsets scrollingInsets;
@property (assign, nonatomic) CGFloat velocity;

@property (assign, nonatomic) UIEdgeInsets scrolableContainerScrollInsets;

@property (weak, nonatomic) UIScrollView *scrolableContainer;

@end
