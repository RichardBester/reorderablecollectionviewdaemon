//
//  ViewController.m
//  ReorderableCollectionViewDeamon
//
//  Created by Sauron Black on 8/20/14.
//  Copyright (c) 2014 ThinkMobiles. All rights reserved.
//

#import "ViewController.h"
#import "ReorderableCollectionView.h"
#import "CollectionViewDecoratedFlowLayout.h"
#import "TCollectionDecoraionViewOk.h"
#import "TCollectionDecoraionViewNotOk.h"
#import "MyTribeCollectionViewCell.h"


@interface ViewController () <CollectionViewDecoratedFlowLayoutDataSource, ReorderableCollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) NSMutableArray *data;

@end

@implementation ViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.data = [@[[@[@"Das Insekt", @"Der Tier", @"Der Vogel"] mutableCopy],
                   [@[@"Die Fliege", @"Die Ente", @"Der Hund", @"Die Katze", @"Die Biene", @"Die Kuh", @"Der Mensch", @"Die Fliege", @"Die Ente", @"Der Hund", @"Die Katze", @"Die Biene", @"Die Kuh", @"Der Mensch"] mutableCopy]] mutableCopy];
    
    [self.collectionView.collectionViewLayout registerClass:[TCollectionDecoraionViewOk class] forDecorationViewOfKind:[TCollectionDecoraionViewOk kindOfView]];
    [self.collectionView.collectionViewLayout registerClass:[TCollectionDecoraionViewNotOk class] forDecorationViewOfKind:[TCollectionDecoraionViewNotOk kindOfView]];
}

#pragma mark - DecoratedCollectionViewFlowLayoutDelegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return [self.data count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.data[section] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MyTribeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[MyTribeCollectionViewCell cellReusableIdentifier] forIndexPath:indexPath];
    
    cell.titleLabel.text = (NSString *)self.data[indexPath.section][indexPath.row];
    
    return cell;
}


- (NSString *)kindOfDecorationViewForSection:(NSInteger)section {
    return section == 0 ? [TCollectionDecoraionViewOk kindOfView]: [TCollectionDecoraionViewNotOk kindOfView];
}

#pragma mark - ReorderableCollectionViewDataSource

- (BOOL)collectionView:(UICollectionView *)collectionView canMoveCellFromIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
    return fromIndexPath.section == 0 ? ([self.data[0] count] > 2): YES;
}

- (void)collectionView:(UICollectionView *)collectionView moveCellFromIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
    NSString *item = self.data[fromIndexPath.section][fromIndexPath.item];
    if (fromIndexPath.section != toIndexPath.section) {
        [self.data[fromIndexPath.section] removeObjectAtIndex:fromIndexPath.item];
        [self.data[toIndexPath.section] insertObject:item atIndex:toIndexPath.item];
    } else {
        self.data[fromIndexPath.section][fromIndexPath.item] = self.data[toIndexPath.section][toIndexPath.item];
        self.data[toIndexPath.section][toIndexPath.item] = item;
    }
}

@end
