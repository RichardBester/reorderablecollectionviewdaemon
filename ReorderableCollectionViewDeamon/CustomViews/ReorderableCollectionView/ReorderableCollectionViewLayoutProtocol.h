//
//  ReorderableCollectionViewLayoutProtocol.h
//  Destroy
//
//  Created by Sauron Black on 8/19/14.
//  Copyright (c) 2014 ThinkMobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ReorderableCollectionViewLayoutProtocol <NSObject>

@required
- (NSInteger)sectionAtPoint:(CGPoint)point;

@end
