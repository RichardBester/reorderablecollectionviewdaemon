//
//  TCollectionViewFlowLayout.m
//  Tribesta
//
//  Created by Sauron Black on 8/18/14.
//  Copyright (c) 2014 ThinkMobiles. All rights reserved.
//

#import "CollectionViewDecoratedFlowLayout.h"

@interface CollectionViewDecoratedFlowLayout ()

@property (strong, nonatomic) NSMutableArray *decorationViewsAttributes;

@end

@implementation CollectionViewDecoratedFlowLayout

- (void)prepareLayout {
    [super prepareLayout];
    
    self.decorationViewsAttributes = [NSMutableArray array];
    
    for (int i = 0; i < [self.collectionView.dataSource numberOfSectionsInCollectionView:self.collectionView]; i++) {
        NSString *kindOfDecorationView = [((id<CollectionViewDecoratedFlowLayoutDataSource>)self.collectionView.dataSource) kindOfDecorationViewForSection:i];
        UICollectionViewLayoutAttributes *attributes = [self layoutAttributesForDecorationViewOfKind:kindOfDecorationView
                                                                                         atIndexPath:[NSIndexPath indexPathForItem:0 inSection:i]];
        if (attributes) {
            [self.decorationViewsAttributes addObject:attributes];
        }
    }
}

#pragma mark - Private

- (CGRect)rectForSection:(NSUInteger)section {
    CGRect sectionRect = CGRectMake(0.f, 0.f, self.collectionViewContentSize.width, self.sectionInset.top + self.sectionInset.bottom);
    NSUInteger numberOfItems = [self.collectionView.dataSource collectionView:self.collectionView numberOfItemsInSection:section];
    
    NSIndexPath *firstItemIndexPath = [NSIndexPath indexPathForItem:0 inSection:section];
    UICollectionViewLayoutAttributes *headerAttributes = [self layoutAttributesForSupplementaryViewOfKind:UICollectionElementKindSectionHeader atIndexPath:firstItemIndexPath];
    UICollectionViewLayoutAttributes *footerAttributes = [self layoutAttributesForSupplementaryViewOfKind:UICollectionElementKindSectionFooter atIndexPath:firstItemIndexPath];
    
    if (numberOfItems > 0) {
        NSIndexPath *lastItemIndexPath = [NSIndexPath indexPathForItem:(numberOfItems - 1) inSection:section];
        
        UICollectionViewLayoutAttributes *firstItemAttribute = [self layoutAttributesForItemAtIndexPath:firstItemIndexPath];
        UICollectionViewLayoutAttributes *lastItemAttribute = [self layoutAttributesForItemAtIndexPath:lastItemIndexPath];
        
        sectionRect.origin.y = headerAttributes ? MIN(CGRectGetMinY(headerAttributes.frame), CGRectGetMinY(firstItemAttribute.frame)): CGRectGetMinY(firstItemAttribute.frame)  - self.sectionInset.top;
        CGFloat maxY = footerAttributes ? MAX(CGRectGetMaxY(footerAttributes.frame), CGRectGetMaxY(lastItemAttribute.frame)): CGRectGetMaxY(lastItemAttribute.frame) + self.sectionInset.bottom;
        
        sectionRect.size.height = maxY - CGRectGetMinY(sectionRect);
    } else if (headerAttributes || footerAttributes) {
        sectionRect.origin.y = headerAttributes ? CGRectGetMinY(headerAttributes.frame): footerAttributes ? CGRectGetMinY(footerAttributes.frame): 0;
        sectionRect.size.height = (footerAttributes ? CGRectGetMaxY(footerAttributes.frame): headerAttributes ? CGRectGetMaxY(headerAttributes.frame): 0) - CGRectGetMinY(sectionRect);
    }
    
    return sectionRect;
}

#pragma mark - Decoration View

- (UICollectionViewLayoutAttributes *)layoutAttributesForDecorationViewOfKind:(NSString *)decorationViewKind atIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForDecorationViewOfKind:decorationViewKind withIndexPath:indexPath];
    attributes.frame = [self rectForSection:indexPath.section];
    attributes.zIndex = -1;
    
    return attributes;
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    NSMutableArray *attributesArray = [NSMutableArray arrayWithArray:[super layoutAttributesForElementsInRect:rect]];
    
    for (UICollectionViewLayoutAttributes *attributes in self.decorationViewsAttributes) {
        if (CGRectIntersectsRect(rect, attributes.frame)) {
            [attributesArray addObject:attributes];
        }
    }
    return attributesArray;
}

#pragma mark - ReorderableCollectionViewLayoutProtocol

- (NSInteger)sectionAtPoint:(CGPoint)point {
    NSInteger section = -1;
    for (int i = 0; (i < [self.collectionView.dataSource numberOfSectionsInCollectionView:self.collectionView]) && (section < 0); i++) {
        if (CGRectContainsPoint([self rectForSection:i], point)) {
            section = i;
        }
    }
    return section;
}

@end
