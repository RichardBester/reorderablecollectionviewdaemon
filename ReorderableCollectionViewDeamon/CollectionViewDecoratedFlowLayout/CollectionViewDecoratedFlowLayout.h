//
//  TCollectionViewFlowLayout.h
//  Tribesta
//
//  Created by Sauron Black on 8/18/14.
//  Copyright (c) 2014 ThinkMobiles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReorderableCollectionViewLayoutProtocol.h"

@protocol CollectionViewDecoratedFlowLayoutDataSource <UICollectionViewDataSource>

@required
- (NSString *)kindOfDecorationViewForSection:(NSInteger)section;

@end

@interface CollectionViewDecoratedFlowLayout : UICollectionViewFlowLayout <ReorderableCollectionViewLayoutProtocol>

@end
