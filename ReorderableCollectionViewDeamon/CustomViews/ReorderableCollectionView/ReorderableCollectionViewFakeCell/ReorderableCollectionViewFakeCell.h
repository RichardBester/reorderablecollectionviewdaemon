//
//  ReorderableCollectionViewFakeCell.h
//  ReorderableCollectionViewDeamon
//
//  Created by Sauron Black on 8/20/14.
//  Copyright (c) 2014 ThinkMobiles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReorderableCollectionViewFakeCell : UIImageView

- (instancetype)initWithCell:(UICollectionViewCell *)cell collectionView:(UICollectionView *)collectionView;

- (void)showInCollectionView:(UICollectionView *)collectionView center:(CGPoint)center;
- (void)removeWithNewCenter:(CGPoint)newCenter completion:(void(^)(void))completion;

@end
