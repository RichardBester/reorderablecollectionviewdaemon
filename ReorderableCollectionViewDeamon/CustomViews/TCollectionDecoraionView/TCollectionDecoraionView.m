//
//  TCollectionDecoraionView.m
//  Fuck
//
//  Created by Sauron Black on 8/18/14.
//  Copyright (c) 2014 ThinkMobile. All rights reserved.
//

#import "TCollectionDecoraionView.h"

@implementation TCollectionDecoraionView

+ (NSString *)kindOfView {
    static NSString *TCollectionDecoraionViewKind = @"TCollectionDecoraionViewKind";
    return TCollectionDecoraionViewKind;
}

#pragma mark - Lifecycle

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createTextLabel];
    }
    return self;
}

#pragma mark - Private

- (void)createTextLabel {
    self.textLabel = [[UILabel alloc] init];
    self.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:53.f];
    
    [self addSubview:self.textLabel];
    self.textLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSArray *hConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|-9.0-[_textLabel]-9.0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_textLabel)];
    NSArray *vConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10.0-[_textLabel(height)]" options:0 metrics:@{@"height": @60}
                                                                      views:NSDictionaryOfVariableBindings(_textLabel)];
    
    [self addConstraints:hConstraints];
    [self addConstraints:vConstraints];
}

@end
